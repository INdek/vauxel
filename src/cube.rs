use {World, RigidBodyHandle};
use three::{
    Geometry,
    Factory,
    Mesh,
    material,
    object::Object,
};

use nalgebra::core::Vector3;
use ncollide::shape::Cuboid;
use nphysics3d::{
    math::Translation,
    object::RigidBody,
};
use synced_object::SyncedObject;


pub struct Cube {
    pub mesh: Mesh,
    body: RigidBodyHandle,
}


impl Cube {
    pub fn new(factory: &mut Factory, world: &mut World) -> Self {
        let size = Vector3::new(1.0, 1.0, 1.0);

        let geometry = Geometry::cuboid(size.x * 2.0, size.y * 2.0, size.z * 2.0);
        let material = material::Basic {
            color: 0xe53935,
            map: None,
        };
        let mesh = factory.mesh(geometry, material);

        let body = world.add_rigid_body(RigidBody::new_dynamic(Cuboid::new(size), 1.0, 0.3, 0.6));

        Cube {
            mesh,
            body
        }
    }

    pub fn update(&mut self) {
        self.sync_transform();
    }

    pub fn set_position<V: Into<[f32; 3]>>(&mut self, pos: V) {
        let vec = pos.into();

        self.mesh.set_position(vec);
        self.body.borrow_mut()
            .set_translation(Translation::new(vec[0], vec[1], vec[2]));
    }
}

impl SyncedObject for Cube {
    fn get_mesh(&mut self) -> &mut Mesh {
        &mut self.mesh
    }

    fn get_body(&self) -> &RigidBodyHandle {
        &self.body
    }
}


// impl Object for Cube {}
//
// impl AsRef<Base> for Cube {
//     fn as_ref(&self) -> &Base {
//         self.mesh.as_ref()
//     }
// }
