use {World};
use three::{
    self,
    Geometry,
    Factory,
    Mesh,
    object::{
        Base,
        Object
    },
};

use nalgebra::core::Vector3;
use ncollide::shape::Cuboid;
use nphysics3d::{
    object::{RigidBody, RigidBodyHandle},
};


pub struct Map {
    pub mesh: Mesh,
    body: RigidBodyHandle<f32>,
}

impl Map {
    pub fn new(factory: &mut Factory, world: &mut World) -> Self {
        let size = Vector3::new(16.0, 1.0, 16.0);

        let geometry = Geometry::cuboid(size.x, size.y, size.z);
        let material = three::material::Basic {
            color: 0x5d99c6,
            map: None,
        };
        let mesh = factory.mesh(geometry, material);

        let rb = RigidBody::new_static(Cuboid::new(size), 0.3, 0.6);
        let body = world.add_rigid_body(rb);

        Map {
            mesh,
            body,
        }
    }
}

impl Object for Map {}

impl AsRef<Base> for Map {
    fn as_ref(&self) -> &Base {
        self.mesh.as_ref()
    }
}
