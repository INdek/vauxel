use {World, RigidBodyHandle};
use three::{
    Geometry,
    Factory,
    Input,
    Mesh,
    material,
    object::Object,
    controls::{FirstPerson, Key},
};

use nalgebra::core::Vector3;
use ncollide::shape::Cuboid;
use nphysics3d::{
    math::Translation,
    object::RigidBody,
};

use synced_object::SyncedObject;

pub struct Player {
    pub mesh: Mesh,
    body: RigidBodyHandle,
}


impl Player {
    pub fn new(factory: &mut Factory, world: &mut World) -> Self {
        let size = Vector3::new(1.0, 1.0, 1.0);

        let geometry = Geometry::cuboid(size.x * 2.0, size.y * 2.0, size.z * 2.0);
        let material = material::Basic {
            color: 0xe53935,
            map: None,
        };
        let mesh = factory.mesh(geometry, material);

        let rb = RigidBody::new_dynamic(Cuboid::new(size), 10.0, 0.3, 0.6);
        let body = world.add_rigid_body(rb);

        Player {
            mesh,
            body
        }
    }

    pub fn handle_input(&mut self, dt: f32, input: &Input) {
        let mut body = self.body.borrow_mut();
        let input_magnitude = 1000.0;
        let mut multiplier = Vector3::from([0.0; 3]);

        if input.hit(Key::W) {
            multiplier += Vector3::new(0.0, 0.0, -1.0);
        }
        if input.hit(Key::S) {
            multiplier += Vector3::new(0.0, 0.0, 1.0);
        }
        if input.hit(Key::A) {
            multiplier += Vector3::new(-1.0, 0.0, 0.0);
        }
        if input.hit(Key::D) {
            multiplier += Vector3::new(1.0, 0.0, 0.0);
        }
        if input.hit(Key::Space) {
            multiplier += Vector3::new(0.0, 1.0, 0.0);
        }

        body.apply_central_impulse(multiplier * input_magnitude * dt);
    }

    pub fn update(&mut self, controls: &mut FirstPerson) {
        let body_pos: [f32; 3] = {
            let body = self.get_body().borrow();
            body.position().translation.vector.into()
        };
        self.sync_transform();
        // self.set_position(body_pos);
        //controls.set_position([body_pos.x, body_pos.y, body_pos.z].into());
        //controls.set_position(body_pos);
    }

    pub fn set_position<V: Into<[f32; 3]>>(&mut self, pos: V) {
        let vec = pos.into();

        self.mesh.set_position(vec);
        self.body.borrow_mut()
            .set_translation(Translation::new(vec[0], vec[1], vec[2]));
    }
}

impl SyncedObject for Player {
    fn get_mesh(&mut self) -> &mut Mesh {
        &mut self.mesh
    }

    fn get_body(&self) -> &RigidBodyHandle {
        &self.body
    }
}

// impl Object for Player {}
//
// impl AsRef<Base> for Player {
//     fn as_ref(&self) -> &Base {
//         self.mesh.as_ref()
//     }
// }
