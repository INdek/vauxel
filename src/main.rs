#[macro_use]
extern crate log;
extern crate env_logger;
extern crate three;
extern crate mint;

extern crate nphysics3d;
extern crate nalgebra;
extern crate ncollide;


mod map;
mod cube;
mod player;
mod synced_object;

use map::Map;
use cube::Cube;
use player::Player;

use nalgebra::core::Vector3;
use three::{
    window::CursorState,
    Object,
};
use std::env;

type World = nphysics3d::world::World<f32>;
type RigidBodyHandle = nphysics3d::object::RigidBodyHandle<f32>;

fn enable_logging() {
    #[cfg(debug_assertions)]
    {
    if let Err(_) = env::var("RUST_LOG") {
        env::set_var("RUST_LOG", format!("{}=trace", env!("CARGO_PKG_NAME")));
    }
    }

    env_logger::init();
}

fn main() {
    enable_logging();

    let mut window = three::Window::new("Getting started with three-rs");
    window.set_cursor_state(CursorState::Grab);
    window.glutin_window().set_cursor_state(CursorState::Grab).unwrap();

    let mut world = World::new();
    world.set_gravity(Vector3::new(0.0, -9.81, 0.0));

    let map = Map::new(&mut window.factory, &mut world);
    map.set_position([0.0, 0.0, 0.0]);
    window.scene.add(&map.mesh);

    let mut cube = Cube::new(&mut window.factory, &mut world);
    cube.set_position([0.0, 10.0, 0.0]);
    window.scene.add(&cube.mesh);

    let mut player = Player::new(&mut window.factory, &mut world);
    player.set_position([0.0, 18.0, 0.0]);
    window.scene.add(&player.mesh);

    // let vertices = vec![
    //     [-0.5, -0.5, -0.5].into(),
    //     [0.5, -0.5, -0.5].into(),
    //     [0.0, 0.5, -0.5].into(),
    // ];
    // let geometry = three::Geometry::with_vertices(vertices);
    // let material = three::material::Basic {
    //     color: 0xFFFF00,
    //     map: None,
    // };
    // let mesh = window.factory.mesh(geometry, material);
    // window.scene.add(&mesh);


    window.scene.background = three::Background::Color(0x000000);

    let ambient_light = window.factory.ambient_light(0xffffffff, 0.5);
	window.scene.add(&ambient_light);

    let mut point_light = window.factory.point_light(0xffffff, 0.9);
	point_light.set_position([-15.0, 10.0, 0.0]);
	window.scene.add(&point_light);

    let mut directional_light = window.factory.directional_light(0xffffff, 1.0);
	window.scene.add(&directional_light);

    let path = concat!(env!("CARGO_MANIFEST_DIR"), "/assets/twisty_cube/2_twisty_cube.gltf");
    println!("Loading");
    let gltf = window.factory.load_gltf(&path);
    println!("Loaded");
	gltf.set_position([0.0, 4.0, 0.0]);
    window.scene.add(&gltf);


    let mut mixer = three::animation::Mixer::new();
    for clip in gltf.clips {
        mixer.action(clip);
    }


    let camera = window.factory.perspective_camera(60.0, 1.0 .. 1000.0);
    let mut controls = three::controls::FirstPerson::builder(&camera)
        .position([3.5, 8.5, 13.0])
        .yaw(-6.6)
        .pitch(0.3)
        .move_speed(10.0)
        // .axis_forward(None)
        // .axis_strafing(None)
        // .axis_vertical(None)
        .build();

    let mut timer = three::Timer::new();
    while window.update() {
        let dt = timer.elapsed();
        timer.reset();

        /*
           /\                 \
           \_|                |
             |  Handle Input  |
             |                |
             |  ______________|
             \_/_______________/
        */

        controls.update(&window.input);
        player.handle_input(dt, &window.input);


        /*
                    \\\///
                   / _  _ \
                 (| (.)(.) |)
           .---.OOOo--()--oOOO.---.
           |                      |
           |       Updates        |
           |                      |
           '---.oooO--------------'
                (   )   Oooo.
                 \ (    (   )
                  \_)    ) /
                        (_/
        */



        world.step(dt);
        //map.update();
        cube.update();
        player.update(&mut controls);

        mixer.update(window.input.delta_time());



        /*       _\|/_
                 (o o)
         +----oOO-{_}-OOo------+
         |                     |
         |                     |
         |       Render        |
         |                     |
         +---------------------*/

        window.render(&camera);
    }
}
