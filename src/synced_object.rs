use RigidBodyHandle;
use three::{Mesh, Object};

/// Syncronizes the position/orientation of an object that has
/// a representation in the physics world and the 3d scene
///
/// Currently we assume the Physics world to be the autorative
/// source of truth
// TODO: This could be generic over `three::Object`
pub trait SyncedObject {
    fn get_mesh(&mut self) -> &mut Mesh;
    fn get_body(&self) -> &RigidBodyHandle;

    fn sync_transform(&mut self) {
        let (rotation, position): ([f32; 4], [f32; 3])  = {
            let body = self.get_body().borrow();
            let pos = body.position();
            let rot = pos.rotation.quaternion().coords;
            ([rot.x, rot.y, rot.z, rot.w], pos.translation.vector.into())
        };

        self.get_mesh().set_transform(position, rotation, 1.0);
    }
}
